# MergeCountries package

## Setup

For setting up the project, You have to download data from [Umpirsky country list project](https://github.com/umpirsky/country-list/tree/master/data) in json format. Choose and download every language You would like to merge into one *.json* file and put them into the *dataSource* folder.

*Note, that this library is node.js as a file dependency.*

## Usage

After every desired *.json* file is in the *dataSource* folder, run the following code from the command line:

```javascript
npm run merge
```

## Finish

You can find the merged json file the *result* folder with the name: *countries.json*. Note, that the result json object is sorted alphabetically, so You might have to manually overwrite the Object Properties before using it. For example, You get the following file:

countries.json
```
{
1:{english names and codes...},
2:{german names and codes...},
3:{italian names and codes...}...
}
```

As You would like to map number 1 to german names, You have to change the numbering like this:
countries.json
```
{
2:{english names and codes...},
1:{german names and codes...},
3:{italian names and codes...}...
}
```

That's it.