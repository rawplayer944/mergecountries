const fs = require("fs");
const dataDir = "./dataSource/";
let countryLists = [];
let countries = {};

console.log("Reading in files from dataSource folder...");
fs.readdirSync(dataDir).forEach(file =>
  countryLists.push(require(`./${dataDir}/${file}`))
);

console.log("Merging started...");
countryLists.forEach((countryList, languageNoIndex) => {
  countries[languageNoIndex + 1] = [];
  for (countryCode in countryList) {
    countries[languageNoIndex + 1].push({
      code: countryCode,
      name: countryList[countryCode]
    });
  }
});
console.log("Merge finished.");
console.log("Creating countries.json...");
const countriesMerged = JSON.stringify(countries);
fs.writeFileSync("./result/countries.json", countriesMerged);
console.log("Countries.json created.");
console.log("Merging process finished. Check *result* folder for the merged json file.");
